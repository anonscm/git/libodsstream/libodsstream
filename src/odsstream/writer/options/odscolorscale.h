/**
 * \file odsstream/writer/options/odscolorscale.h
 * \date 10/03/2018
 * \author Olivier Langella
 * \brief structure to apply a color scale in ODS sheets
 */

/*******************************************************************************
 * Copyright (c) 2013 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the libodsstream library.
 *
 *    libodsstream is a library to read and write ODS documents as streams
 *    Copyright (C) 2013  Olivier Langella <Olivier.Langella@u-psud.fr>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *by the Free Software Foundation, either version 3 of the License, or (at your
 *option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#ifndef ODSCOLORSCALE_H
#define ODSCOLORSCALE_H

#include <QString>
#include <QColor>
#include <QXmlStreamWriter>

class ContentXml;

class OdsColorScale
{
  friend ContentXml;

  public:
  OdsColorScale(const QString &cell_start_position,
                const QString &cell_stop_position);
  OdsColorScale(const OdsColorScale &other);
  virtual ~OdsColorScale();

  /** @brief set cell range to apply color scale
   * @param cell_start_position cell position to start (Feuille1.C2)
   * @param cell_stop_position cell position to stop (Feuille1.D4)
   */
  void setCellRange(const QString &cell_start_position,
                    const QString &cell_stop_position);

  /** @brief set minimum color of the color scale
   * @param minimum_color minimum color
   */
  void setMinimumColor(const QColor &minimum_color);

  /** @brief set maximum color of the color scale
   * @param maximum_color maximum color
   */
  void setMaximumColor(const QColor &maximum_color);

  protected:
  bool isInSheet(const QString &sheet_name);
  void writeConditionalFormat(QXmlStreamWriter *p_writer);

  private:
  std::tuple<QString, QString> parseCellRange(const QString &cell_position);

  private:
  QString _sheet_name;
  QString _cell_start_position;
  QString _cell_stop_position;
  // by default : temperature color scale
  QColor _minimum_color    = QColor("#0000ff");
  QColor _maximum_color    = QColor("#ff0000");
  QColor _percentile_color = QColor("#ffffff");
};

#endif // ODSCOLORSCALE_H
