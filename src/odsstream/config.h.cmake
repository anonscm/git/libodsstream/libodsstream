
#pragma once

#cmakedefine LIBODSSTREAM_VERSION "@LIBODSSTREAM_VERSION@"

#cmakedefine LIBODSSTREAM_LIB_NAME "@LIBODSSTREAM_LIB_NAME@"

#include <QDebug>


enum class TsvSeparator {tab, comma, semicolon};

