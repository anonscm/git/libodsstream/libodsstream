/**
 * \file odsstream/qtablewriter.cpp
 * \date 15/03/2019
 * \author Olivier Langella
 * \brief write an entire table sheet from a QAbstractModel
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the libodsstream library.
 *
 *    libodsstream is a library to read and write ODS documents as streams
 *    Copyright (C) 2019  Olivier Langella <Olivier.Langella@u-psud.fr>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *by the Free Software Foundation, either version 3 of the License, or (at your
 *option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "qtablewriter.h"
#include <QDebug>

QtableWriter::QtableWriter(CalcWriterInterface *p_writer,
                           const QAbstractProxyModel *p_table_model)
  : mp_writer(p_writer), mp_tableModel(p_table_model)
{
  for(int i = 0; i < mp_tableModel->columnCount(); i++)
    {
      m_percentColumns.insert(std::pair<int, bool>(i, false));
    }
}

void
QtableWriter::setFormatPercentForColumn(const QString &column_title)
{
  int col_number = mp_tableModel->columnCount();
  for(int j = 0; j < col_number; j++)
    {

      QVariant head_var =
        mp_tableModel->headerData(j, Qt::Horizontal, Qt::DisplayRole);
      if(head_var.toString() == column_title)
        {
          m_percentColumns[j] = true;
          qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
                   << " col=" << j;
        }
    }
}

void
QtableWriter::setFormatPercentForColumn(const QModelIndex &column_index)
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
           << " col=" << column_index.column();
  QModelIndex nIndex = mp_tableModel->mapFromSource(column_index);

  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
           << " col=" << nIndex.column();
  m_percentColumns[nIndex.column()] = true;
}

void
QtableWriter::writeSheet(const QString &sheetName)
{
  mp_writer->writeSheet(sheetName);
  OdsTableSettings settings;
  // settings.setHorizontalWindowSplit(4);
  settings.setVerticalSplit(1);
  mp_writer->setCurrentOdsTableSettings(settings);

  int col_number = mp_tableModel->columnCount();

  int line_number = mp_tableModel->rowCount();
  bool ok         = false;
  for(int j = 0; j < col_number; j++)
    {

      QVariant head_var =
        mp_tableModel->headerData(j, Qt::Horizontal, Qt::ToolTipRole);

      if(head_var.isNull())
        {
        }
      else
        {
          mp_writer->setCellAnnotation(head_var.toString());
        }
      head_var = mp_tableModel->headerData(j, Qt::Horizontal, Qt::DisplayRole);
      if(head_var.isNull())
        {
          mp_writer->writeEmptyCell();
        }
      else
        {
          mp_writer->writeCell(head_var.toString());
        }
    }

  for(int i = 0; i < line_number; i++)
    {
      mp_writer->writeLine();
      for(int j = 0; j < col_number; j++)
        {
          QModelIndex index = mp_tableModel->index(i, j);
          QVariant var      = mp_tableModel->data(index, Qt::CheckStateRole);
          if(var.isNull())
            {
              var = mp_tableModel->data(index, Qt::DisplayRole);

              ok = false;
              if(var.isNull())
                {
                  mp_writer->writeEmptyCell();
                }
              else
                {
                  double var_float = var.toDouble(&ok);
                  if(ok)
                    {
                      if(m_percentColumns[j])
                        {
                          mp_writer->writeCellPercentage(var_float);
                        }
                      else
                        {
                          mp_writer->writeCell(var_float);
                        }
                    }
                  else
                    {
                      mp_writer->writeCell(var.toString());
                    }
                }
            }
          else
            {
              if(var == Qt::Checked)
                {
                  mp_writer->writeCell(true);
                }
              else
                {
                  mp_writer->writeCell(false);
                }
            }
        }
    }
}
