configure_file (${CMAKE_SOURCE_DIR}/src/odsstream/config.h.cmake ${CMAKE_SOURCE_DIR}/src/odsstream/config.h)


set(CPP_FILES 
	odsstream/reader/odscell.cpp
	odsstream/odsexception.h 
	odsstream/saxreader/saxhandlercontentxml.cpp 
	odsstream/odsdocreader.cpp
	odsstream/writer/options/odscolorscale.cpp
	odsstream/writer/options/odstablecellstyle.cpp
	odsstream/writer/options/odstablecellstyleref.cpp
	odsstream/writer/options/odstablesettings.cpp
	odsstream/writer/structure/stylesxml.cpp
	odsstream/writer/structure/contentxml.cpp
	odsstream/writer/structure/metaxml.cpp
	odsstream/writer/structure/settingsxml.cpp
	odsstream/writer/structure/manifestxml.cpp
	odsstream/odsdocwriter.cpp
	odsstream/qtablewriter.cpp
	odsstream/tsvdirectorywriter.cpp
	odsstream/tsvoutputstream.cpp
	odsstream/tsvreader.cpp
	)



set(ODS2CSV_CPP_FILES 
	ods2csv.cpp
	)

set(TSV2ODS_CPP_FILES 
	tsv2ods.cpp
	)



configure_file (${CMAKE_SOURCE_DIR}/src/config.h.cmake ${CMAKE_SOURCE_DIR}/src/config.h)

message ("LIBODSSTREAM_LIB_SOVERSION: ${LIBODSSTREAM_LIB_SOVERSION}")
#qt5-default qtbase5-dev
find_package( Qt5Core REQUIRED )
find_package( Qt5Xml REQUIRED )
find_package( Qt5Gui REQUIRED )

# Build the static lib
add_library(odsstream-static STATIC ${CPP_FILES})

target_include_directories (odsstream-static PUBLIC 
	${QUAZIP_INCLUDE_DIR}
	${ZLIB_INCLUDE_DIR}
	)

set_target_properties(odsstream-static
	PROPERTIES OUTPUT_NAME odsstream
	CLEAN_DIRECT_OUTPUT 1
	)

target_link_libraries(odsstream-static
	Qt5::Core
	Qt5::Xml
	Qt5::Gui
	${QUAZIP_LIBRARIES}
	)

# Build the shared lib
add_library(odsstream-shared SHARED ${CPP_FILES})

target_include_directories (odsstream-shared PUBLIC 
	${QUAZIP_INCLUDE_DIR}
	${ZLIB_INCLUDE_DIR}
	)

set_target_properties(odsstream-shared PROPERTIES
	OUTPUT_NAME odsstream
	VERSION ${LIBODSSTREAM_LIB_VERSION}
	SOVERSION ${LIBODSSTREAM_LIB_SOVERSION}
	)


target_link_libraries(odsstream-shared
	Qt5::Core
	Qt5::Xml
	Qt5::Gui
	${QUAZIP_LIBRARIES}
	)


# Install libs

install(TARGETS odsstream-shared 
	LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
	# This one is for WIN32
	RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
	)

#if (WIN32 OR _WIN32)
	#install(TARGETS odsstream-shared RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})
#else ()
	#install(TARGETS odsstream-shared LIBRARY DESTINATION  ${CMAKE_INSTALL_LIBDIR})
#endif()

install(TARGETS odsstream-static 
	ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR})

add_executable(ods2csv ${ODS2CSV_CPP_FILES})

target_include_directories (ods2csv PUBLIC
	"${CMAKE_SOURCE_DIR}/src"
	)

set_target_properties(ods2csv
	PROPERTIES OUTPUT_NAME ods2tsv
	CLEAN_DIRECT_OUTPUT 1
	)

target_link_libraries(ods2csv odsstream-shared
	Qt5::Core
	)

install(PROGRAMS ${CMAKE_BINARY_DIR}/src/ods2tsv DESTINATION bin)


add_executable(tsv2ods ${TSV2ODS_CPP_FILES})

target_include_directories (tsv2ods PUBLIC
	"${CMAKE_SOURCE_DIR}/src"
	)

set_target_properties(tsv2ods
	PROPERTIES OUTPUT_NAME tsv2ods
	CLEAN_DIRECT_OUTPUT 1
	)


target_link_libraries(tsv2ods odsstream-shared
	Qt5::Core
	)

install(PROGRAMS ${CMAKE_BINARY_DIR}/src/tsv2ods DESTINATION bin)


# Install headers
install(DIRECTORY odsstream/ DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/odsstream 
	FILES_MATCHING PATTERN "*.h"
	PATTERN "*.hpp")

