# Copyright : Olivier Langella (CNRS)
# License : GPL-3.0+
# Authors : Olivier Langella

FIND_PATH( ODSSTREAM_INCLUDE_DIR odsstream/odsdocreader.h
               PATHS /usr/local/include /usr/include
               PATH_SUFFIXES odsstream libodsstream ENV PATH)

FIND_LIBRARY(ODSSTREAM_QT4_LIBRARY NAMES odsstream-qt4)
IF (ODSSTREAM_INCLUDE_DIR AND ODSSTREAM_QT4_LIBRARY)
  MESSAGE(STATUS "XXXXXXXXXXXXX ${ODSSTREAM_LIBRARY} XXXXXXXXXXXXXXX")
  SET(ODSSTREAM_QT4_FOUND TRUE)
ENDIF (ODSSTREAM_INCLUDE_DIR AND ODSSTREAM_QT4_LIBRARY)

IF (ODSSTREAM_QT4_FOUND)
  # show which CppUnit was found only if not quiet
  IF (NOT ODSSTREAM_FIND_QUIETLY)
    MESSAGE(STATUS "Found ODSSTREAM_QT4_LIBRARY: ${ODSSTREAM_QT4_LIBRARY}")
  ENDIF (NOT ODSSTREAM_FIND_QUIETLY)
ENDIF (ODSSTREAM_QT4_FOUND)


FIND_LIBRARY(ODSSTREAM_QT5_LIBRARY NAMES odsstream-qt5)
IF (ODSSTREAM_INCLUDE_DIR AND ODSSTREAM_QT5_LIBRARY)
  MESSAGE(STATUS "XXXXXXXXXXXXX ${ODSSTREAM_LIBRARY} XXXXXXXXXXXXXXX")
  SET(ODSSTREAM_QT5_FOUND TRUE)
ENDIF (ODSSTREAM_INCLUDE_DIR AND ODSSTREAM_QT5_LIBRARY)

IF (ODSSTREAM_QT5_FOUND)
  # show which CppUnit was found only if not quiet
  IF (NOT ODSSTREAM_FIND_QUIETLY)
    MESSAGE(STATUS "Found ODSSTREAM_QT5_LIBRARY: ${ODSSTREAM_QT5_LIBRARY}")
  ENDIF (NOT ODSSTREAM_FIND_QUIETLY)
ENDIF (ODSSTREAM_QT5_FOUND)

# fatal error if CppUnit is required but not found
IF (NOT ODSSTREAM_QT4_FOUND)
  IF (NOT ODSSTREAM_QT5_FOUND)
    # fatal error if CppUnit is required but not found
    IF (PAPPSOMSPP_FIND_REQUIRED)
      MESSAGE(FATAL_ERROR "Could not find ODSSTREAM Qt4 or Qt5 please do specify the ODSSTREAM_INCLUDE_DIR, ODSSTREAM_QT4_LIBRARY and  ODSSTREAM_QT5_LIBRARY variables using ccmake!")
    ENDIF (PAPPSOMSPP_FIND_REQUIRED)
  ENDIF (NOT ODSSTREAM_QT5_FOUND)
ENDIF (NOT ODSSTREAM_QT4_FOUND)
