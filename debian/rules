#!/usr/bin/make -f
# -*- makefile -*-

# Gets the name of the source package
DEB_SOURCE_PACKAGE := $(strip $(shell egrep '^Source: ' debian/control | cut -f 2 -d ':'))

# Gets the full version of the source package including debian version
DEB_VERSION := $(shell dpkg-parsechangelog | egrep '^Version:' | cut -f 2 -d ' ')
DEB_NOEPOCH_VERSION := $(shell echo $(DEB_VERSION) | cut -d: -f2-)
# Gets only the upstream version of the package
DEB_UPSTREAM_VERSION := $(shell echo $(DEB_NOEPOCH_VERSION) | sed 's/-[^-]*$$//')

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1
export DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)
#export LIBODSSTREAM_VERSION="0.7.6"


%:
	dh $@ --buildsystem=cmake


override_dh_clean:
	dh_clean 	
	-$(RM) libodsstream0.links libodsstream-dev.links

override_dh_auto_configure:
	dh_auto_configure -- -DLIB_SUFFIX="/$(DEB_HOST_MULTIARCH)" \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_BUILD_TYPE=Release \
		-DMAKE_TEST=1 \
		-DCMAKE_SKIP_RPATH=1

	sed -e "s/@DEB_HOST_MULTIARCH@/$(DEB_HOST_MULTIARCH)/g" \
		-e "s/@LIBODSSTREAM_VERSION@/$(DEB_UPSTREAM_VERSION)/g" \
		< debian/libodsstream0.links.in \
		> debian/libodsstream0.links
	
	sed -e "s/@DEB_HOST_MULTIARCH@/$(DEB_HOST_MULTIARCH)/g" \
		-e "s/@LIBODSSTREAM_VERSION@/$(DEB_UPSTREAM_VERSION)/g" \
		< debian/libodsstream-dev.links.in \
		> debian/libodsstream-dev.links

override_dh_auto_build:
	dh_auto_build --no-parallel
	dh_auto_build -- doc
